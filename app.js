var express     = require("express"),
    bodyParser  = require("body-parser"),
    path        = require("path"),
    ejs         = require("ejs"),
    app         = express();

var port = process.env.PORT || 3000;

var index = require("./routes/index");



app.set("view engine", "ejs");

app.use(bodyParser.urlencoded({extended:true}));
app.use(express.static(path.join(__dirname, "public")));


app.use("/", index);

app.listen(port, process.env.IP);
    

    